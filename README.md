# Goals Of This Api

Provide a simple and modular way for the front end website to interact with the trading back end. 

# Inputs to API

This API takes in a User Identification,Command, and Confirmation 

## User Identification

The user identification has not been decided yet, since that would depend on how facebook api works

##Comand

The three options for a Command are:

* Buy Request
* Sell Request
* Get User Specific Open Orders	and rating				



##Confirmation of Order Sent

This will simply provide a way of ensuring that the order has been taken and that it can be removed from the books


# Outputs of API

+ Raw Data: For The purpose of creating a user interface that shows the market
+ Matches Alert: To Alert the Front End of orders that match
+ User Specific Orders:So that a User can see what orders they have open on the market
+ Confirmation of Input Commands: To confirm with the user that the command has been completed

#Specifications
+ Will use [Websockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications)

#Notes On Project
+ Will use Price/Time priority [Trading Algorithm](https://stackoverflow.com/questions/13112062/which-are-the-order-matching-algorithms-most-commonly-used-by-electronic-financi)
 
+ We should limit the amount of orders that a person should have open , like to one or two.
This would prevent people from flooding the market, and or market manipulation. we should also combine this with a rating system. 
+ Connection to front end server should be done via pgp encryption or some other form. 
+We also need a rating system to ensure reliable transactions


#Questions on Project

The fact that this is a peer to peer exchange presents several issues, since the user dose not have cj's held up in the system.

Here are some questions I have concerning this Issue

Do we Want the User orders to expire or last forever?




